function CreateStore(initState) {
    let currentState = initState;
    const listeners = new Set();
    return {
        state: (filter) => {
            if (!filter) {
                return currentState;
            } else {
                return currentState[filter];
            }
        },
        setState: (newValue) => {
            currentState = newValue;
            listeners.forEach((listener) => listener(currentState));
        },
        subscribe: (listener) => {
            listeners.add(listener);
            return () => listeners.delete(listener);
        },
    };
}

export default CreateStore;

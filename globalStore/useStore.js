import React, { useState, useEffect } from 'react';

const useStore = (store = { state: () => {}, setState: () => {} }, stateFilter = '') => {
    const [state, setState] = useState(Destructing(store.state(), stateFilter));
    useEffect(() => store.subscribe((state) => setState(Destructing(state, stateFilter))), []);
    return state;
};

function Destructing(state, destructor) {
    try {
        if (!destructor) return state;
        if (typeof destructor != 'string') throw 'destructor must be string';
        const [currDestructor, ...restDestructor] = destructor.split('.');
        if (restDestructor.length === 0) return state[currDestructor];
        return Destructing(state[currDestructor], restDestructor.join('.'));
    } catch (error) {
        if (error.message === "Cannot read properties of undefined (reading 'undefined')") return undefined;
        // console.log('UseStore destructing Error', error.message);
        return 'Destructing Error' + error.message;
    }
}

export function NestedUpdate(obj, destructor, updateValue) {
    try {
        const properties = Array.isArray(destructor) ? destructor : destructor.split('.');
        const propName = properties.shift();
        if (!obj[propName]) {
            obj[propName] = {};
        }
        if (properties.length === 0) {
            obj[propName] = updateValue;
        } else {
            obj[propName] = NestedUpdate(obj[propName], properties, updateValue);
        }
        return obj;
    } catch (error) {
        return 'Nested Update Error ' + error.message;
    }
}

export default useStore;

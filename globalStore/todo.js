import { uuid } from '@/helper/uuid';
import CreateStore from './CreateStore';

const initState = {
    name: '',
    todoList: [],
};

const store = CreateStore(initState);

const Action = () => {
    let updateStore = (key, value) => {
        store.setState({ ...store.state(), [key]: value });
    };
    let updateName = (value) => {
        updateStore('name', value);
    };
    let clearInput = () => {
        updateStore('name', '');
    };
    let addTodo = (item) => {
        if (!item) return;
        let newItem = { id: uuid(), content: item };
        let updatedList = [...store.state('todoList'), newItem];
        updateStore('todoList', updatedList);
    };
    let deleteItem = (id) => {
        const filteredList = store.state('todoList').filter((item) => item.id !== id);
        updateStore('todoList', filteredList);
    };
    return { updateName, clearInput, addTodo, deleteItem };
};

export const todoAction = Action();
export default store;

import { uuid } from '@/helper/uuid';
import CreateStore from './CreateStore';
import { NestedUpdate } from './useStore';

const initState = {
    payload: {
        name: { first: 'teddy', last: 'yee' },
        age: 50,
        food: [
            { type: 'japanese', price: 50, origin: ['tokyo', 'osaka'] },
            { type: 'western', price: 100, origin: ['USA', 'Italy'] },
        ],
    },
};

const store = CreateStore(initState);

const Action = () => {
    const UpdateStore = (newPayload) => {
        store.setState({
            ...store.state(),
            ...newPayload,
        });
    };

    const updatePayload = (destructor, newValue) => {
        let updatedState = NestedUpdate(store.state('payload'), destructor, newValue);
        UpdateStore({ payload: updatedState });
    };
    return { updatePayload };
};

export const nestedAction = Action();
export default store;

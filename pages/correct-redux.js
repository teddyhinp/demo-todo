import Counter from '@/component/Counter';
import DisplayTodo from '@/redux-component/DisplayTodo';
import Header from '@/redux-component/Header';
import NameBlock from '@/redux-component/NameBlock';
import TodoBlock from '@/redux-component/TodoBlock';
import { store } from '@/redux/store';
import { Box } from '@mui/material';
import { styled } from '@mui/system';
import React from 'react';
import { Provider } from 'react-redux';

export default function Correct() {
    return (
        <Provider store={store}>
            <Header />
            <PageContainer>
                <InputBlock>
                    <NameBlock />
                    <TodoBlock />
                </InputBlock>
                <DisplayTodo />
            </PageContainer>
        </Provider>
    );
}

const PageContainer = styled(Box)({
    display: 'flex',
    flexDirection: 'column',
    gap: '20px',
    height: '100%',
    marginTop: '20px',
    padding: '20px',
});

const InputBlock = styled(Box)({
    display: 'flex',
    gap: '10px',
    alignItems: 'center',
});

import Header from '@/component/Header';
import { TextField, Box, Button, InputAdornment } from '@mui/material';
import React, { useState } from 'react';
import CancelIcon from '@mui/icons-material/Cancel';
export default function WrongSignlePage() {
    let [name, setName] = useState('');
    let [todoInput, setTodoInput] = useState('');
    let [todoList, setTodoList] = useState([]);
    //single page state will re-render the whole page for every setState, including the header
    return (
        <>
            <Header />
            {/* messy in-line sx */}
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '20px',
                    height: '100%',
                    marginTop: '20px',
                    padding: '20px',
                }}
            >
                {/* messy in-line sx */}
                <Box sx={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
                    <Box sx={{ width: '200px' }}>Your name: {name}</Box>
                    <TextField
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        placeholder="Your name"
                        InputProps={{
                            endAdornment: (
                                <InputAdornment
                                    position="end"
                                    //   inline handler
                                    onClick={() => {
                                        setName('');
                                    }}
                                >
                                    <CancelIcon />
                                </InputAdornment>
                            ),
                        }}
                    />
                    <TextField
                        value={todoInput}
                        onChange={(e) => setTodoInput(e.target.value)}
                        placeholder="todo"
                        InputProps={{
                            endAdornment: (
                                <InputAdornment
                                    position="end"
                                    //   inline handler
                                    onClick={() => {
                                        setTodoInput('');
                                    }}
                                >
                                    <CancelIcon />
                                </InputAdornment>
                            ),
                        }}
                    />
                    <Button
                        variant="contained"
                        //   inline handler
                        onClick={() => {
                            setTodoList([...todoList, todoInput]);
                            setTodoInput('');
                        }}
                    >
                        Add
                    </Button>
                </Box>

                {todoList.length > 0
                    ? todoList.map((el, i) => (
                          <Box key={i} sx={{ display: 'flex', alignItems: 'center' }}>
                              <Box>{el}</Box>
                              <Button
                                  //   inline handler
                                  onClick={() => {
                                      const filteredList = todoList.filter((item) => item !== el);
                                      setTodoList(filteredList);
                                  }}
                              >
                                  Delete
                              </Button>
                          </Box>
                      ))
                    : 'No todo item'}
            </Box>
        </>
    );
}

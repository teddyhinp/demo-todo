import Counter from '@/component/Counter';
import DisplayTodo from '@/component/DisplayTodo';
import Header from '@/component/Header';
import NameBlock from '@/component/NameBlock';
import NestedExample from '@/component/NestedExample';
import TodoBlock from '@/component/TodoBlock';
import { Box } from '@mui/material';
import { styled } from '@mui/system';
import React from 'react';

export default function Correct() {
    return (
        <>
            <Header />
            <PageContainer>
                <InputBlock>
                    <NameBlock />
                    <TodoBlock />
                </InputBlock>
                <DisplayTodo />
                <Counter />
                {/* <NestedExample /> */}
            </PageContainer>
        </>
    );
}

const PageContainer = styled(Box)({
    display: 'flex',
    flexDirection: 'column',
    gap: '20px',
    height: '100%',
    marginTop: '20px',
    padding: '20px',
});

const InputBlock = styled(Box)({
    display: 'flex',
    gap: '10px',
    alignItems: 'center',
});

import { styled } from '@mui/system';
import { useRouter } from 'next/router';

export default function Home() {
    let router = useRouter();
    const navagate = (route) => {
        router.push(`/${route}`);
    };
    return (
        <ButtonContainer>
            <button onClick={() => navagate('correct')}>Correct</button>
            <button onClick={() => navagate('correct-redux')}>Correct-redux</button>
            <button onClick={() => navagate('wrong-single-page-state')}>Wrong:Single Page State</button>
            <button onClick={() => navagate('wrong-props-hell')}>Wrong:Props Hell</button>
        </ButtonContainer>
    );
}

const ButtonContainer = styled('div')({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100vh',
    gap: '10px',
});

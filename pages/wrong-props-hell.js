import Header from '@/component/Header';
import { TextField, Box, Button, InputAdornment } from '@mui/material';
import React, { useState } from 'react';
import CancelIcon from '@mui/icons-material/Cancel';
export default function WrongPropsHell() {
    let [name, setName] = useState('');
    let [todoInput, setTodoInput] = useState('');
    let [todoList, setTodoList] = useState([]);
    return (
        <>
            <Header />
            {/* state should be init from child instead of parent */}
            <Content
                name={name}
                setName={setName}
                todoInput={todoInput}
                setTodoInput={setTodoInput}
                setTodoList={setTodoList}
                todoList={todoList}
            />
        </>
    );
}

function Content({ name, setName, todoInput, setTodoInput, setTodoList, todoList }) {
    return (
        // messy in-line sx
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                gap: '20px',
                height: '100%',
                marginTop: '20px',
                padding: '20px',
            }}
        >
            {/* messy in-line sx */}
            <Box
                sx={{
                    display: 'flex',
                    gap: '10px',
                    alignItems: 'center',
                }}
            >
                {/* messy in-line sx */}
                <Box
                    sx={{
                        width: '200px',
                    }}
                >
                    Your name: {name}
                </Box>
                {/* passing props again (2layer)*/}
                <NameInput name={name} setName={setName} />
                {/* passing props again (2layer)*/}
                <TodoInput todoInput={todoInput} setTodoInput={setTodoInput} />
                {/* passing 4 props again (2layer)*/}
                <AddTodoButton
                    setTodoList={setTodoList}
                    todoList={todoList}
                    todoInput={todoInput}
                    setTodoInput={setTodoInput}
                />
            </Box>
            {/* passing props again (2layer)*/}
            <DisplayTodo todoList={todoList} setTodoList={setTodoList} />
        </Box>
    );
}

function NameInput({ name, setName }) {
    return (
        <TextField
            value={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Your name"
            //inline unclear logic
            InputProps={
                name
                    ? {
                          endAdornment: (
                              <InputAdornment
                                  position="end"
                                  //   inline handler
                                  onClick={() => {
                                      setName('');
                                  }}
                              >
                                  <CancelIcon />
                              </InputAdornment>
                          ),
                      }
                    : null
            }
        />
    );
}

function TodoInput({ todoInput, setTodoInput }) {
    return (
        <TextField
            value={todoInput}
            onChange={(e) => setTodoInput(e.target.value)}
            placeholder="todo"
            InputProps={
                //inline unclear logic
                todoInput
                    ? {
                          endAdornment: (
                              <InputAdornment
                                  position="end"
                                  onClick={() => {
                                      //   inline handler
                                      setTodoInput('');
                                  }}
                              >
                                  <CancelIcon />
                              </InputAdornment>
                          ),
                      }
                    : null
            }
        />
    );
}

function AddTodoButton({ setTodoList, todoList, todoInput, setTodoInput }) {
    return (
        <Button
            variant="contained"
            //   inline handler
            onClick={() => {
                setTodoList([...todoList, todoInput]);
                setTodoInput('');
            }}
        >
            Add
        </Button>
    );
}

function DisplayTodo({ todoList, setTodoList }) {
    return (
        <Box>
            {todoList.length > 0
                ? todoList.map((el, i) => (
                      <Box
                          key={i}
                          // messy in-line sx
                          sx={{
                              display: 'flex',
                              alignItems: 'center',
                          }}
                      >
                          <Box>{el}</Box>
                          <Button
                              //   inline handler
                              onClick={() => {
                                  const filteredList = todoList.filter((item) => item !== el);
                                  setTodoList(filteredList);
                              }}
                          >
                              Delete
                          </Button>
                      </Box>
                  ))
                : 'No todo item'}
        </Box>
    );
}

import React from 'react';
import { styled } from '@mui/system';
import { Box } from '@mui/material';
import CastForEducationIcon from '@mui/icons-material/CastForEducation';
export default function Header() {
    return (
        <HeaderContainer>
            <Box>This is a Header</Box>
            <RightBox>
                Component Refactoring Tutorial
                <CastForEducationIcon />
            </RightBox>
        </HeaderContainer>
    );
}

const HeaderContainer = styled(Box)({
    display: 'flex',
    justifyContent: 'space-between',
    background: '#ffb81c',
    height: '30px',
    padding: '20px 10px',
});

const RightBox = styled(Box)({
    display: 'flex',
    alignItems: 'center',
    gap: '20px',
});

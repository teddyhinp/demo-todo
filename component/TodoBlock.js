import { todoAction } from '@/globalStore/todo';
import { InputAdornment, TextField, Button } from '@mui/material';
import React, { useState } from 'react';
import CancelIcon from '@mui/icons-material/Cancel';

export default function TodoBlock() {
    let [todoInput, setTodoInput] = useState('');

    let handleOnChange = (e) => {
        setTodoInput(e.target.value);
    };
    let handleClearInput = () => {
        setTodoInput('');
    };
    let handleAddToList = () => {
        todoAction.addTodo(todoInput);
        setTodoInput('');
    };
    let displayCancelBtn = () => {
        let isVisible = todoInput ? 'visable' : 'hidden';
        return {
            endAdornment: (
                <InputAdornment onClick={handleClearInput} position="end" sx={{ visibility: isVisible }}>
                    <CancelIcon />
                </InputAdornment>
            ),
        };
    };
    return (
        <>
            <TextField value={todoInput} onChange={handleOnChange} placeholder="todo" InputProps={displayCancelBtn()} />
            <Button variant="contained" onClick={handleAddToList}>
                Add
            </Button>
        </>
    );
}

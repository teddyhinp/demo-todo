import { Box, Button } from '@mui/material';
import { styled } from '@mui/system';
import React, { useState } from 'react';

export default function Counter() {
    let [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount((prev) => prev + 1);
    };
    const handleMinus = () => {
        setCount((prev) => prev - 1);
    };

    return (
        <CounterContainer isCountGreaterThan3={count > 3}>
            <Box>count: {count}</Box>
            <Button onClick={handleAdd} variant="outlined">
                +
            </Button>
            <Button onClick={handleMinus} variant="outlined">
                -
            </Button>
        </CounterContainer>
    );
}

const CounterContainer = styled(Box, {
    shouldForwardProp: (prop) => prop != 'isCountGreaterThan3',
})(({ isCountGreaterThan3 }) => ({
    display: 'flex',
    alignItems: 'center',
    gap: '10px',
    padding: '10px',
    backgroundColor: isCountGreaterThan3 ? 'red' : 'yellow',
}));

const WrongContainer = styled(Box)(({ isCountGreaterThan3 }) => ({
    display: 'flex',
    alignItems: 'center',
    gap: '10px',
    padding: '10px',
    backgroundColor: isCountGreaterThan3 ? 'red' : 'yellow',
}));

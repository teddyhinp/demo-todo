import todoStore, { todoAction } from '@/globalStore/todo';
import useStore from '@/globalStore/useStore';
import { Box, InputAdornment, TextField } from '@mui/material';
import React from 'react';
import CancelIcon from '@mui/icons-material/Cancel';

export default function NameBlock() {
    let name = useStore(todoStore, 'name');
    let displayCancelBtn = () => {
        let isVisible = name ? 'visable' : 'hidden';
        return {
            endAdornment: (
                <InputAdornment onClick={handleClearInput} position="end" sx={{ visibility: isVisible }}>
                    <CancelIcon />
                </InputAdornment>
            ),
        };
    };

    let handleOnChange = (e) => {
        todoAction.updateName(e.target.value);
    };
    let handleClearInput = () => {
        todoAction.clearInput();
    };

    return (
        <>
            <Box width="200px">Your name: {name}</Box>
            <TextField value={name} onChange={handleOnChange} placeholder="Your name" InputProps={displayCancelBtn()} />
        </>
    );
}

import React from 'react';
import nestedStore, { nestedAction } from '@/globalStore/nested';
import useStore from '@/globalStore/useStore';
export default function NestedStore() {
    let payload = useStore(nestedStore, 'payload');
    let name = useStore(nestedStore, 'payload.name');
    let age = useStore(nestedStore, 'payload.age');
    let firstName = useStore(nestedStore, 'payload.name.first');
    let lastName = useStore(nestedStore, 'payload.name.last');
    let firstFoodOrigin = useStore(nestedStore, 'payload.food.0.origin');
    let thirdFood = useStore(nestedStore, 'payload.food.2');

    const handleOnClick = () => {
        let newValue = { type: 'chinese', price: 50, origin: ['Sichuan', 'Beijing'] };
        nestedAction.updatePayload('food.2', newValue);
    };

    return (
        <div>
            <button onClick={handleOnClick}>click</button>
        </div>
    );
}

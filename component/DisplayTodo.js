import todoStore, { todoAction } from '@/globalStore/todo';
import useStore from '@/globalStore/useStore';
import { Box, Button } from '@mui/material';
import { styled } from '@mui/system';
import React from 'react';

export default function DisplayTodo() {
    let todoList = useStore(todoStore, 'todoList');
    let handleDeleteItem = (id) => {
        todoAction.deleteItem(id);
    };

    return todoList.length > 0
        ? todoList.map((el, i) => (
              <TodoItem key={i} sx={{}}>
                  <Box>{el.content}</Box>
                  <Button onClick={() => handleDeleteItem(el.id)}>Delete</Button>
              </TodoItem>
          ))
        : 'No todo item';
}

const TodoItem = styled(Box)({
    display: 'flex',
    alignItems: 'center',
});

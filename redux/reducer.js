import { combineReducers } from 'redux';
import todoReducer from './todoReducer';

let rootReducer = combineReducers({
    todoReducer,
});

export default rootReducer;

import { uuid } from '@/helper/uuid';
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    name: '',
    todoList: [],
};

const todoSlice = createSlice({
    name: 'todo',
    initialState,
    reducers: {
        updateName: (state, action) => {
            state.name = action.payload;
        },
        clearInput: (state) => {
            state.name = '';
        },
        addTodo: (state, action) => {
            if (!action.payload) return;
            let newItem = { id: uuid(), content: action.payload };
            let updatedList = [...state.todoList, newItem];
            state.todoList = updatedList;
        },
        deleteItem: (state, action) => {
            const filteredList = state.todoList.filter((item) => item.id !== action.payload);
            state.todoList = filteredList;
        },
    },
});

export const { updateName, clearInput, addTodo, deleteItem } = todoSlice.actions;
export default todoSlice.reducer;

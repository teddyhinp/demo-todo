import { Box, InputAdornment, TextField } from '@mui/material';
import React from 'react';
import CancelIcon from '@mui/icons-material/Cancel';
import { useSelector, useDispatch } from 'react-redux';
import { clearInput, updateName } from '@/redux/todoReducer';

export default function NameBlock() {
    let name = useSelector((state) => state.todoReducer.name);
    let dispatch = useDispatch();

    let displayCancelBtn = () => {
        let isVisible = name ? 'visable' : 'hidden';
        return {
            endAdornment: (
                <InputAdornment onClick={handleClearInput} position="end" sx={{ visibility: isVisible }}>
                    <CancelIcon />
                </InputAdornment>
            ),
        };
    };

    let handleOnChange = (e) => {
        dispatch(updateName(e.target.value));
    };
    let handleClearInput = () => {
        dispatch(clearInput());
    };

    return (
        <>
            <Box width="200px">Your name: {name}</Box>
            <TextField value={name} onChange={handleOnChange} placeholder="Your name" InputProps={displayCancelBtn()} />
        </>
    );
}

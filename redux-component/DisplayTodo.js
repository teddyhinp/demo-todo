import { deleteItem } from '@/redux/todoReducer';
import { Box, Button } from '@mui/material';
import { styled } from '@mui/system';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
export default function DisplayTodo() {
    let todoList = useSelector((state) => state.todoReducer.todoList);
    let dispatch = useDispatch();

    // let todoList = useStore(todoStore, 'todoList');
    let handleDeleteItem = (id) => {
        dispatch(deleteItem(id));
    };

    return todoList.length > 0
        ? todoList.map((el, i) => (
              <TodoItem key={i} sx={{}}>
                  <Box>{el.content}</Box>
                  <Button onClick={() => handleDeleteItem(el.id)}>Delete</Button>
              </TodoItem>
          ))
        : 'No todo item';
}

const TodoItem = styled(Box)({
    display: 'flex',
    alignItems: 'center',
});
